import numpy as np
import torch
import torchvision
import os, sys, random
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from PIL import Image
import xml.etree.ElementTree as ET
from scipy.ndimage import rotate

img_base_path = "./data/PCB_DATASET/images/Missing_hole/"
images = os.listdir(img_base_path)

annotations_path = './data/PCB_DATASET/Annotations/Missing_hole/'
annotations = os.listdir('./data/PCB_DATASET/Annotations/Missing_hole/')

img = random.randint(0,154)

annotation_path = annotations[img]
image_path = annotation_path
image_path =image_path[:-4] +  image_path[-1:-3] + '.jpg'
bb = os.path.join(annotations_path, annotation_path)

tree = ET.parse(bb)
root = tree.getroot()
objects = root.findall('object')
list_of_boxes = []
for obj in objects:
    bndbox = obj.find('bndbox')
    xmin = int(bndbox.find('xmin').text)
    ymin = int(bndbox.find('ymin').text)
    width = int(bndbox.find('xmax').text) - xmin
    height = int(bndbox.find('ymax').text) - ymin
    box = [xmin, ymin, width, height]
    list_of_boxes.append(box)

img = Image.open(os.path.join(img_base_path, image_path))

fig, ax = plt.subplots()
for box in list_of_boxes:
    rect = patches.Rectangle((box[0], box[1]), box[2], box[3], linewidth=1, edgecolor='r', facecolor='none')
    ax.add_patch(rect)
ax.imshow(img)
plt.show()
