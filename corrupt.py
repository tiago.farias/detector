import numpy as np
import matplotlib.pyplot as plt
import cv2

def corrupt_noise(img, y, x, height, width):
    noise = 255*np.random.uniform(size=(height, width,3))
    new_img = 1*img
    new_img[x:x+height, y:y+width,:] = noise
    return new_img

img = cv2.imread('box.jpeg')
corrupt_img = corrupt_noise(img, 480, 300, 100, 100)

fig, axs = plt.subplots(1,2)
axs[0].imshow(img)
axs[1].imshow(corrupt_img)
plt.show()




